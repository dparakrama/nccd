package BasicPageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class BaisicPageObjects {
	protected static WebDriver driver;

	public BaisicPageObjects(WebDriver driver) {
		this.driver = driver;
	}

	/***
	 * Service Method for Text boxes
	 * 
	 * @throws InterruptedException
	 * **/

	public void enterText(String TextboxXpath, String Text)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TextboxXpath))).clear();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(TextboxXpath))).sendKeys(Text);
	}

	/***
	 * Service Method for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public static void clickButton(String ButtonXpath) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(ButtonXpath))).click();
	}

	/***
	 * Service Methods for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickButtonbyCss(String ButtonCSS) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(ButtonCSS))).click();
	}



	public void getContent(String Labelxpath, String Text) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String content = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Labelxpath))).getText();
		//Assert.assertTrue(content.contains(Text));
	}
	
	
	
	
	public void getContentbyAttribute(String Labelxpath, String Text) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String content = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Labelxpath))).getAttribute("value").toString();
		//Assert.assertTrue(content.contains(Text));
		
	}
	/***
	 * Service Method for tables and finding values in a table
	 * 
	 * @throws InterruptedException
	 * **/

	public void findTableElement(String tableXpath, String searchStringValue,
			String firstXpathBeforRowNumber, String LastXpathBeforeRowNumber)
			throws InterruptedException {
		boolean breakIt = true;
		while (true) {
			breakIt = true;
			try {
				WebDriverWait wait = new WebDriverWait(driver, 30);
				List<WebElement> rows_table = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(tableXpath))).findElements(By
						.tagName("tr"));
				int rows_count = rows_table.size();

				for (int row = 0; row < rows_count; row++) {
					List<WebElement> Columns_row = rows_table.get(row)
							.findElements(By.tagName("td"));
					int columns_count = Columns_row.size();

					for (int column = 0; column < columns_count; column++) {
						String celltext = Columns_row.get(column).getText();
						if (celltext.contains(searchStringValue)) {
							driver.findElement(
									By.xpath(firstXpathBeforRowNumber + row
											+ LastXpathBeforeRowNumber))
									.click();
							Thread.sleep(4000);
						}

					}
				}
			} catch (Exception e) {
				if (e.getMessage().contains("ELEMENT IS NOT ATTACHED")) {
					breakIt = false;
				}
			}
			if (breakIt) {
				break;
			}
		}

	}
	
	public void isDisplayed (String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		Assert.assertTrue(wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).isDisplayed());
		
	}
	public void isElementAvailable(String xpath, String text) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String content = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).getText();
		Assert.assertTrue(content.contains(text));
		
	}
	
	public void isElementNotAvailable (String xpath){
	Assert.assertEquals(0, driver.findElements(By.xpath(xpath)).size());
	}
	

	
	public String getContent(String xpath) {
	return driver.findElement(By.xpath(xpath)).getText();
	}
}
